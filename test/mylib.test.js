const expect = require("chai").expect;
const mylib = require("../src/mylib");

// Function to run before testing starts
before(function () {
    console.log('This function runs before the tests start.');
});
  
  // Function to run after testing is completed
after(function () {
    console.log('This function runs after all tests are completed.');
});

describe("Testing mylib", () => {
    it("Can add 1 and 1 together", () => {
        const result = mylib.addition(1, 1);
        expect(result).to.equal(2);
    });
    it("Can substarct 1 from 1", () => {
        const result1 = mylib.substarction(1, 1);
        expect(result1).to.equal(0);
    });
    it("Can multiply 5 and 6 together", () => {
        const result2 = mylib.multiplication(5, 6);
        expect(result2).to.equal(30);
    });
    it("Expect to throw an error", () => {
        const result3 = mylib.division(4, 0);
        expect(functionCall).to.throw(Error, 'This is an error message');
    })
})