const addition = (a, b) => { // addition function
    const sum = a + b;
    return sum;
};

const substarction = (a, b) => { // substarction function
    const result = a - b;
    return result;
};

const multiplication = (a, b) => { // multiplication function
    const product = a * b;
    return product;
};

const division = (a, b) => { // division function
    if (b == 0) {
        throw new Error("Can't devise from zero!"); // in case of division by 0
    }
    const remainder = a / b;
    return remainder;
};

module.exports = { // export functions
    addition : addition,
    substarction : substarction,
    multiplication : multiplication,
    division : division
}